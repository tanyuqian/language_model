num_epochs = 1000
num_steps = 35

l2_decay = 1e-5
lr_decay = 0.1

data_path = 'simple-examples/data/'

training_batch_size = 64
valid_batch_size = 16
test_batch_size = 16
