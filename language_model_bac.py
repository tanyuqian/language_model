from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import tensorflow as tf
import texar as tx
import numpy as np

import os
import argparse

import hyperparameters_bac as hp
from ptb_reader import prepare_data, ptb_iterator
from embedding_tied_language_model import EmbeddingTiedLanguageModel

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--num_layers', default=1, type=int)

args = arg_parser.parse_args()

opt_vars = {
    'learning_rate': 0.003,
    'best_valid': 50000000.,
    'steps_not_improved': 0
}


def fetch_data_batches():
    return prepare_data(hp.data_path)


def embedding_drop(embedding_matrix, keep_prob):
    mask = tf.nn.dropout(tf.ones((embedding_matrix.shape[0], 1)), keep_prob)
    return mask * embedding_matrix


def optimize(output_logits, data_batch):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_ids'],
        logits=output_logits,
        sequence_length=[hp.num_steps] * output_logits.shape[0])

    l2_loss = 0
    for element in tf.trainable_variables():
        l2_loss += tf.nn.l2_loss(element)

    global_step = tf.Variable(0, name='global_step', trainable=True)
    learning_rate = \
        tf.placeholder(dtype=tf.float32, shape=(), name='learning_rate')

    optimizer = tf.train.AdamOptimizer(
        learning_rate=learning_rate,
        beta1=0.,
        beta2=0.999,
        epsilon=1e-9)
    train_op = optimizer.minimize(
        mle_loss + hp.l2_decay * l2_loss, global_step=global_step)

    return mle_loss, global_step, learning_rate, train_op


def train(epoch_num, data):
    training_log = \
        open(LOG_DIR + 'training_log_epoch{}.txt'.format(epoch_num), 'w')

    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.training_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.training_batch_size, hp.num_steps),
            dtype=tf.int32)

        model = EmbeddingTiedLanguageModel(vocab_size=data['vocab_size'])
        initial_state, output_logits, final_state = \
            model(data_batch['text_ids'],
                  [hp.num_steps] * hp.training_batch_size)

        mle_loss, global_step, learning_rate, train_op = \
            optimize(output_logits, data_batch)

        saver = tf.train.Saver(tf.global_variables(), max_to_keep=1)
        with tf.Session() as sess:
            if epoch_num == 0:
                sess.run(tf.global_variables_initializer())
            else:
                saver.restore(
                    sess, LOG_DIR + 'checkpoints/training.ckpt_epoch{}'.format(
                        epoch_num - 1))
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            data_iter = ptb_iterator(
                data["train_text_id"], hp.training_batch_size, hp.num_steps)

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for step, (text_ids, target_ids) in enumerate(data_iter):
                feed_dict = {
                    data_batch['text_ids']: text_ids,
                    data_batch['target_ids']: target_ids,
                    learning_rate: opt_vars['learning_rate'],
                    tx.global_mode(): tf.estimator.ModeKeys.TRAIN}

                for i, (c, h) in enumerate(initial_state):
                    feed_dict[c] = cur_state[i].c
                    feed_dict[h] = cur_state[i].h

                _, cur_mle_loss, cur_global_step,\
                cur_learning_rate, cur_state = sess.run(
                    [train_op, mle_loss, global_step,
                     learning_rate, final_state], feed_dict=feed_dict)

                cur_avg_mle_loss = cur_mle_loss / hp.num_steps
                ppl = np.exp(cur_avg_mle_loss)
                print('mle_loss:', cur_mle_loss, '  ppl:', ppl,
                      '  global_step:', cur_global_step,
                      '  learning_rate:', cur_learning_rate,
                      file=training_log)
                training_log.flush()

                if cur_global_step % 100 == 0:
                    saved_path = saver.save(
                        sess, LOG_DIR + 'checkpoints/training.ckpt_gl{}'.format(
                            cur_global_step))
                    valid_ppl = valid(saved_path, data)
                    test_ppl = test(saved_path, data)

                    print('global_step:', cur_global_step,
                          '    learning_rate:', cur_learning_rate,
                          '    valid ppl:', valid_ppl,
                          '    test ppl:', test_ppl,
                          file=results_file)
                    results_file.flush()

                    if valid_ppl < opt_vars['best_valid']:
                        opt_vars['best_valid'] = valid_ppl
                        opt_vars['steps_not_improved'] = 0
                    else:
                        opt_vars['steps_not_improved'] += 1

                    if opt_vars['steps_not_improved'] >= 30:
                        opt_vars['steps_not_improved'] = 0
                        opt_vars['learning_rate'] *= hp.lr_decay

            saved_path = saver.save(
                sess, LOG_DIR +
                      'checkpoints/training.ckpt_epoch{}'.format(epoch_num))


def valid(model_path, data):
    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)

        model = EmbeddingTiedLanguageModel(vocab_size=data['vocab_size'])
        initial_state, output_logits, final_state = \
            model(data_batch['text_ids'],
                  [hp.num_steps] * hp.valid_batch_size)
        mle_loss, global_step, learning_rate, train_op = \
            optimize(output_logits, data_batch)

        result_list = []
        with tf.Session() as sess:
            saver = tf.train.Saver(tf.global_variables())
            saver.restore(sess, model_path)
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            data_iter = ptb_iterator(
                data["valid_text_id"], hp.valid_batch_size, hp.num_steps)

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for step, (text_ids, target_ids) in enumerate(data_iter):
                feed_dict = {
                    data_batch['text_ids']: text_ids,
                    data_batch['target_ids']: target_ids,
                    learning_rate: opt_vars['learning_rate'],
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT}

                for i, (c, h) in enumerate(initial_state):
                    feed_dict[c] = cur_state[i].c
                    feed_dict[h] = cur_state[i].h

                cur_mle_loss, cur_state = sess.run(
                    [mle_loss, final_state], feed_dict=feed_dict)
                result_list.append(cur_mle_loss)

        return np.exp(sum(result_list) / float(len(result_list)) / hp.num_steps)


def test(model_path, data):
    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)

        model = EmbeddingTiedLanguageModel(vocab_size=data['vocab_size'])
        initial_state, output_logits, final_state = \
            model(data_batch['text_ids'],
                  [hp.num_steps] * hp.test_batch_size)
        mle_loss, global_step, learning_rate, train_op = \
            optimize(output_logits, data_batch)

        result_list = []
        with tf.Session() as sess:
            saver = tf.train.Saver(tf.global_variables())
            saver.restore(sess, model_path)
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            data_iter = ptb_iterator(
                data["test_text_id"], hp.test_batch_size, hp.num_steps)

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for step, (text_ids, target_ids) in enumerate(data_iter):
                feed_dict = {
                    data_batch['text_ids']: text_ids,
                    data_batch['target_ids']: target_ids,
                    learning_rate: opt_vars['learning_rate'],
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT}

                for i, (c, h) in enumerate(initial_state):
                    feed_dict[c] = cur_state[i].c
                    feed_dict[h] = cur_state[i].h

                cur_mle_loss, cur_state = sess.run(
                    [mle_loss, final_state], feed_dict=feed_dict)
                result_list.append(cur_mle_loss)

        return np.exp(sum(result_list) / float(len(result_list)) / hp.num_steps)


def main():
    data = fetch_data_batches()

    for i in range(hp.num_epochs):
        train(i, data)
        if i >= 2:
            os.system('rm ' + LOG_DIR +
                      'checkpoints/training.ckpt_epoch{}*'.format(i - 2))


if __name__ == '__main__':
    LOG_DIR = 'training_logs_testtie/'.format(args.num_layers)
    os.system('mkdir ' + LOG_DIR)

    results_file = open(LOG_DIR + 'results.txt', 'w')
    main()