from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import tensorflow as tf
import texar as tx
import numpy as np

import os
import argparse

import hyperparameters as hp
from ptb_reader import prepare_data, ptb_iterator


def fetch_data_batches():
    return prepare_data(hp.data_path)


def embedding_drop(embedding_matrix, keep_prob):
    mask = tf.nn.dropout(tf.ones((embedding_matrix.shape[0], 1)), keep_prob)
    return mask * embedding_matrix


def decode(data, data_batch):
    from wd_lstm_cell import WeightDropLSTMCell

    cell_list = []
    for i in range(hp.num_layers):
        cell_list.append(tf.nn.rnn_cell.DropoutWrapper(
            WeightDropLSTMCell(
                num_units=hp.hidden_size if i < hp.num_layers - 1
                else hp.embedding_dim,
                weight_keep_prob=tx.utils.switch_dropout(
                    1. - hp.weight_dropout)),
            input_keep_prob=tx.utils.switch_dropout(
                1. - hp.intra_layer_dropout) if i != 0 else 1.,
            variational_recurrent=True,
            input_size=hp.hidden_size if i > 0 else hp.embedding_dim,
            dtype=tf.float32))
    cell = tf.nn.rnn_cell.DropoutWrapper(
        cell=tf.nn.rnn_cell.MultiRNNCell(cells=cell_list),
        input_keep_prob=tx.utils.switch_dropout(1. - hp.input_dropout),
        output_keep_prob=tx.utils.switch_dropout(1. - hp.output_dropout),
        state_keep_prob=tx.utils.switch_dropout(1. - hp.state_dropout),
        variational_recurrent=True,
        input_size=hp.embedding_dim,
        dtype=tf.float32)

    output_layer = tf.layers.Dense(units=data['vocab_size'])
    output_layer(tf.ones((1, hp.embedding_dim)))

    embedding_matrix = tf.transpose(output_layer.weights[0])
    embedding_matrix = embedding_drop(
        embedding_matrix, tx.utils.switch_dropout(1. - hp.embedding_dropout))

    decoder = tx.modules.BasicRNNDecoder(
        cell=cell, vocab_size=data['vocab_size'], output_layer=output_layer)

    initial_state = decoder.zero_state(
        data_batch['target_ids'].shape[0], dtype=tf.float32)
    outputs, final_state, sequence_length = decoder(
        inputs=tf.nn.embedding_lookup(
            embedding_matrix, data_batch['text_ids']),
        initial_state=initial_state,
        impute_finished=True,
        decoding_strategy="train_greedy",
        sequence_length=[hp.num_steps] * data_batch['target_ids'].shape[0])

    return initial_state, outputs.logits, final_state


def optimize(output_logits, data_batch):
    mle_loss = tx.losses.sequence_sparse_softmax_cross_entropy(
        labels=data_batch['target_ids'],
        logits=output_logits,
        sequence_length=[hp.num_steps] * output_logits.shape[0],
        average_across_timesteps=True,
        sum_over_timesteps=False)

    l2_loss = 0
    for element in tf.trainable_variables():
        if element.name.find('bias') == -1:
            l2_loss += tf.nn.l2_loss(element)

    global_step = tf.Variable(0, name='global_step', trainable=True)
    train_op = tx.core.get_train_op(
        mle_loss + hp.l2_decay * l2_loss,
        global_step=global_step,
        hparams={
            "optimizer": {
                "type": "AdamOptimizer",
                "kwargs": {"learning_rate": hp.learning_rate}
            },
            "gradient_clip": {
                "type": "clip_by_global_norm",
                "kwargs": {"clip_norm": hp.gradient_clip}
            }
        })

    return mle_loss, global_step, train_op


def train(data):
    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.training_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.training_batch_size, hp.num_steps),
            dtype=tf.int32)

        initial_state, output_logits, final_state = decode(data, data_batch)
        mle_loss, global_step, train_op = optimize(output_logits, data_batch)

        saver = tf.train.Saver(tf.global_variables(), max_to_keep=1)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for epoch in range(hp.num_epochs):
                training_log = open(
                    LOG_DIR + 'training_log_epoch{}.txt'.format(epoch), 'w')

                mle_losses = []

                data_iter = ptb_iterator(
                    data["train_text_id"], hp.training_batch_size, hp.num_steps)

                for step, (text_ids, target_ids) in enumerate(data_iter):
                    feed_dict = {
                        data_batch['text_ids']: text_ids,
                        data_batch['target_ids']: target_ids,
                        tx.global_mode(): tf.estimator.ModeKeys.TRAIN}

                    for i, (c, h) in enumerate(initial_state):
                        feed_dict[c] = cur_state[i].c
                        feed_dict[h] = cur_state[i].h

                    _, cur_mle_loss, cur_global_step, cur_state = sess.run(
                        [train_op, mle_loss, global_step, final_state],
                        feed_dict=feed_dict)
                    mle_losses.append(cur_mle_loss)

                    ppl = np.exp(cur_mle_loss)
                    print('mle_loss:', cur_mle_loss, '  ppl:', ppl,
                          '  global_step:', cur_global_step,
                          file=training_log)
                    training_log.flush()

                    if step % hp.log_interval == 0:
                        ppl = np.exp(sum(mle_losses) / float(len(mle_losses)))
                        print('step {} training ppl = {}'.format(step, ppl),
                              file=results_file)
                        results_file.flush()

                saved_path = saver.save(
                    sess, LOG_DIR +
                          'checkpoints/training.ckpt_epoch{}'.format(epoch))
                print('-' * 100, file=results_file)

                valid_ppl = valid(saved_path, data)
                test_ppl = test(saved_path, data)

                print('Epoch {}:     valid ppl: {}     test ppl: {}'.format(
                    epoch, valid_ppl, test_ppl), file=results_file)
                print('-' * 100, file=results_file)
                results_file.flush()


def valid(model_path, data):
    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)

        initial_state, output_logits, final_state = decode(data, data_batch)
        mle_loss, global_step, train_op = optimize(output_logits, data_batch)

        result_list = []
        with tf.Session() as sess:
            saver = tf.train.Saver(tf.global_variables())
            saver.restore(sess, model_path)
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            data_iter = ptb_iterator(
                data["valid_text_id"], hp.valid_batch_size, hp.num_steps)

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for step, (text_ids, target_ids) in enumerate(data_iter):
                feed_dict = {
                    data_batch['text_ids']: text_ids,
                    data_batch['target_ids']: target_ids,
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT}

                for i, (c, h) in enumerate(initial_state):
                    feed_dict[c] = cur_state[i].c
                    feed_dict[h] = cur_state[i].h

                cur_mle_loss, cur_state = sess.run(
                    [mle_loss, final_state], feed_dict=feed_dict)
                result_list.append(cur_mle_loss)

        return np.exp(sum(result_list) / float(len(result_list)))


def test(model_path, data):
    graph = tf.Graph()
    with graph.as_default():
        data_batch = {}
        data_batch['text_ids'] = tf.placeholder(
            name='text_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)
        data_batch['target_ids'] = tf.placeholder(
            name='target_ids',
            shape=(hp.valid_batch_size, hp.num_steps),
            dtype=tf.int32)


        initial_state, output_logits, final_state = decode(data, data_batch)
        mle_loss, global_step, train_op = optimize(output_logits, data_batch)

        result_list = []
        with tf.Session() as sess:
            saver = tf.train.Saver(tf.global_variables())
            saver.restore(sess, model_path)
            sess.run(tf.local_variables_initializer())
            sess.run(tf.tables_initializer())

            data_iter = ptb_iterator(
                data["test_text_id"], hp.test_batch_size, hp.num_steps)

            cur_state = sess.run(
                initial_state,
                feed_dict={tx.global_mode(): tf.estimator.ModeKeys.PREDICT})

            for step, (text_ids, target_ids) in enumerate(data_iter):
                feed_dict = {
                    data_batch['text_ids']: text_ids,
                    data_batch['target_ids']: target_ids,
                    tx.global_mode(): tf.estimator.ModeKeys.PREDICT}

                for i, (c, h) in enumerate(initial_state):
                    feed_dict[c] = cur_state[i].c
                    feed_dict[h] = cur_state[i].h

                cur_mle_loss, cur_state = sess.run(
                    [mle_loss, final_state], feed_dict=feed_dict)
                result_list.append(cur_mle_loss)

        return np.exp(sum(result_list) / float(len(result_list)))


def main():
    data = fetch_data_batches()
    train(data)


if __name__ == '__main__':
    LOG_DIR = 'training_logs_wd/'
    os.system('mkdir ' + LOG_DIR)

    results_file = open(LOG_DIR + 'results.txt', 'w')
    main()
