num_epochs = 1000

training_batch_size = 64
valid_batch_size = 1
test_batch_size = 1
num_steps = 35

init_lr = 0.003
l2_decay = 1e-5
lr_decay = 0.1