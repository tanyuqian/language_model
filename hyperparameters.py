num_epochs = 1000
embedding_dim = 400
num_steps = 30

intra_layer_dropout = 0.25
input_dropout = 0.4
output_dropout = 0.4
state_dropout = 0.0
embedding_dropout = 0.1
weight_dropout = 0.5

num_layers = 3
hidden_size = 1150

l2_decay = 1.2e-6
learning_rate = 0.001
gradient_clip = 0.25

data_path = 'simple-examples/data/'

training_batch_size = 64
valid_batch_size = 64
test_batch_size = 64

log_interval = 200

# dropout=0.4,
# dropoute=0.1,
# dropouth=0.25,
# dropouti=0.4,
# wdrop=0.5
